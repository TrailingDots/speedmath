"""
    The constants containing directory and filenames.
"""

import os

if os.environ.get('IS_TESTING', False):
    # The default location of speed trainer config files.
    # These config files contain deliberately broken
    # config files for testing purposes.
    # These config files exist in the tests dir.
    _speed_math_config_dir = os.path.expanduser("/speed_math/tests")
else:
    # The default location of speed trainer config files.
    # This .speed_math config dir should be the final dir.
    # However, due to pip problems, use the local dir.
    # This assumed the user runs the code in the same
    # dir as the distro.
    # _speed_math_config_dir = os.path.expanduser("~/.speed_math/")
    _speed_math_config_dir = os.path.expanduser("/speed_math/config")

# Convert to absolute dir name
speed_math_config_dir = os.getcwd() + _speed_math_config_dir

# Program version
version = "0.2.0"

# Make that path absolute
SPEED_MATH_CONFIG_DIR = os.path.abspath(speed_math_config_dir) + '/'

# The default config filename.
DEFAULT_CONFIG_FILE = SPEED_MATH_CONFIG_DIR + 'default.yaml'

def constants_str():
    return (f"Version {version}\nConfig file set to: '{DEFAULT_CONFIG_FILE}'\n")
        
def print_constants():
    "Print the config constants for assistance in user settings."
    print(constants_str())

