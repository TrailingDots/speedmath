#!/bin/env python3
"""Perform speed_math training"""

import argparse
import datetime
import os
import sys
import time

import constants

from exercise import Exercise
from speed_config import SpeedConfig
from speed_config import Ok, Fail, Status, Values

# All exceptions use TrainerException as a base class.
from speed_exceptions import TrainerException
from speed_exceptions import TrainerOnlyOneCommandPerSessionPermitted
from speed_exceptions import TrainerNoCommandInCommandLine
import speed_exceptions


class Trainer():
    """Perform the speed math training."""

    def __init__(self, user_args, parser):
        """Given the user_arg_dict built from the command
        line args, run the training session.

        The user_arg looks like:
            {'config': 'add_3.yaml', 'addition': True,
             'multiplication': False}
        """

        self.user_args = user_args
        self.parser = parser
        # A seed allows users to reproduce a session.
        self.seed = self._set_random_number_generator()
        self.config = SpeedConfig(self.user_args, self.parser)
        self.log_file = None    # Where session stats get stored.

    def _set_random_number_generator(self):
        """Set the random number generator for problem generation
        to either the current time in millisecnods or to
        the user supplied values from the '--seed' option.
        """
        # Time in milliseconds
        time_ms = int(time.time())
        the_seed = self.user_args.get('seed', time_ms)
        if the_seed == '0':
            the_seed = time_ms
        return the_seed

    def write_header_if_no_log_file(self):
        """ Write a header if the log file does not exist.
        If the log file exists, just return.
        If the log file does not exists, write a header
        line that contains the column names.
        """
        if os.path.exists(self.log_file):
            return
        # Get the operation from the user args. There is only one.
        oper = [key for (key, operation) in self.user_args.items()
                if operation]
        header = f"timestamp,config,{oper[1]},right,wrong,seed,time" "\n"
        with open(self.log_file, 'w') as lfh:
            lfh.write(header)

    def log_session(self, values):
        """Log the values of the session"""

        default_log_file = 'speedy.csv'
        self.log_file = self.config.default_config['common']. \
            get('log_file', default_log_file)
        
        # Write a header if the log file does not exist
        self.write_header_if_no_log_file()

        with open(self.log_file, 'a') as lfh:
            num_right, num_wrong, time_delta = values
            dt_obj = datetime.datetime.now()

            # Format of log file date:
            #   YearMonthDay:HourSeconds
            # This makes it easy to sort by time.
            timestamp_str = dt_obj.strftime("%Y%m%dT%H%M%S")

            # Get the operation from the user args. There is only one.
            oper = [key for (key, operation) in self.user_args.items()
                    if operation]
            config_file = self.user_args['config']
            if not config_file:   # If no user suppied config, use default
                config_file = self.user_args['default_config_file']

            oper_str = oper[1]
            csv_line = (f"{timestamp_str},"
                        f"{config_file},"
                        f"{oper_str},"
                        f"{num_right},"
                        f"{num_wrong},"
                        f"{self.seed},"
                        f"{time_delta:.2f}" 
                        "\n")
            lfh.write(csv_line)

    def do_addition(self):
        """Perform one addition set of problems."""

        this_exercise = Exercise(self.config.default_config)
        results = this_exercise.run_addition_problems()

        # If the addition is configured wrong, bail
        if results.status == Fail:
            return results
        self.log_session(results.values)
        return results

    def do_subtraction(self):
        """Perform one subtraction set of problems."""

        this_exercise = Exercise(self.config.default_config)
        results = this_exercise.run_subtraction_problems()

        # If the subtraction is configured wrong, bail
        if results.status == Fail:
            return results
        self.log_session(results.values)
        return results

    def do_multiplication(self):
        """Perform one multiplication set of problems."""

        this_exercise = Exercise(self.config.default_config)
        results = this_exercise.run_multiplication_problems()

        # If the multiplication is configured wrong, bail
        if results.status == Fail:
            return results
        self.log_session(results.values)
        return results

    def do_division(self):
        """Perform one division set of problems."""

        this_exercise = Exercise(self.config.default_config)
        results = this_exercise.run_division_problems()

        # If the division is configured wrong, bail
        if results.status == Fail:
            return results
        self.log_session(results.values)
        return results

    def user_operation(self, parser):
        """Determine the user request operation from the
        args in the command line.

        Returns a named tuple of Status with Values:
            Status(Ok, 
                   Values(num_right, num_wrong, time_delta),
                   None)
        """

        user_args = self.config.user_args

        # Find the first arg set to True.
        # Yes, value == True MUST be used since we compare
        # to True!
        command = [key for (key, value) in user_args.items()
                   if (key not in ('config', 'list_configs')) \
                       and value is True]

        # One and only one command in the user command line
        # args permitted.
        # Because this routine gets called in the __init__,
        # there is nothing to return.
        # The error is fatal, so raise is Ok to use.
        if len(command) == 0:
            #parser.print_help()
            raise TrainerNoCommandInCommandLine(
                TrainerNoCommandInCommandLine.__doc__)
        if len(command) > 1:
            #parser.print_help()
            raise TrainerOnlyOneCommandPerSessionPermitted(
                TrainerOnlyOneCommandPerSessionPermitted.__doc__)
        operations = {
            "addition": self.do_addition,
            "subtraction": self.do_subtraction,
            "multiplication": self.do_multiplication,
            "division": self.do_division,
        }

        # Execute the requested operation and return results
        op_fcn = operations.get(command[0], None)
        results = op_fcn()
        return results   # Returns Status named tuple


def build_user_arg_dict(caller_argv):
    """Parse the command line arguments.

    Passing argv allows unit tests to pass a wider and
    easier unit testing.

    In non-unittest, i.e. normal training, sys.argv gets
    passed.

    Returns the param dictionary and the parser.
    The parser can be used to produce help messages
    in case of errors.
    """

    import textwrap
    parser = argparse.ArgumentParser(
            prog='Speed Math Trainer',
            formatter_class=argparse.RawDescriptionHelpFormatter,
            # Provide examples of how to run the trainer.
            epilog=textwrap.dedent('''\
                Usage Examples:
                    ./speed_math.sh --help
                    ./speed_math.sh --addition -c add_3.yaml
                    ./speed_math.sh --subtraction -c sub_3.yaml
                    ./speed_math.sh --multplication -c mult_11_12.yaml
                    ./speed_math.sh --division -c div_by_divit.yaml
                    ./speed_math.sh --list  # List configs
                    ./speed_math.sh --seed 1234567  # Seed random numbers

                Same as above but with shortened options:
                    ./speed_math.sh -h
                    ./speed_math.sh -a -c add_3.yaml
                    ./speed_math.sh -s -c sub_3.yaml
                    ./speed_math.sh -m -c mult_11_12.yaml
                    ./speed_math.sh -d -c div_by_divit.yaml
                    ./speed_math.sh -l  # List configs
            '''))

    parser.add_argument('-c', '--config',
                        metavar='config',
                        type=str,
                        action="store",
                        required=False,
                        help='Configuration file name')
    parser.add_argument('-a', '--addition',
                        action="store_true",
                        help="Run the addition training",
                        required=False)
    parser.add_argument('-s', "--subtraction",
                        action="store_true",
                        help="Run the subtraction training",
                        required=False)
    parser.add_argument('-m', '--multiplication',
                        action="store_true",
                        help="Run the multiplation training",
                        required=False)
    parser.add_argument('-d', "--division",
                        action="store_true",
                        help="Run the division training",
                        required=False)
    parser.add_argument('--default_config_file',
                        type=str,
                        action='store',
                        required=False,
                        default='default.yaml',
                        help='Default configuration file name')
    parser.add_argument('--seed',
                        type=str,
                        default='0',
                        required=False,
                        action='store',
                        help='Seed to set random number generator')
    parser.add_argument('-l', "--list_configs",
                        required=False,
                        action='store_const',
                        const=True,     # simple flag arg
                        help='Flag for list config files title')
    args = parser.parse_args(args=caller_argv[1:])   # skip prog name

    # Convert the argparser params into a standard dict
    # The resulting dict contains all the params of
    # the command line.
    user_arg_dict = {}
    for key, value in vars(args).items():
        user_arg_dict[key] = value

    return (user_arg_dict, parser)


def top_level_trainer(argv):
    """
    The top-level calls to run a training session.
    """
    (user_arg_dict, parser) = build_user_arg_dict(argv)
    constants.print_constants()
    atrainer = Trainer(user_arg_dict, parser)
    status = atrainer.user_operation(parser)
    return status


def validate_setup():
    """
    At this point the version of python > 3, so run.
    """
    the_version = sys.version_info
    if the_version.major < 3:
        print("\n\t*** Upgrade to latest python 3 ***\n")
        return Error(Exception("Cannot proceed. Upgrade to latest python 3"))
    return True


def main(argv):
    """
    Top level routine to start the training and capture
    an exceptions.
    Lower level routines will report error conditions
    more thoroughly so simply trap these so the command
    line does not appear to barf.

    Returns:
        0 for Status(Ok, None, None) for Ok run
        1 for Status(Fail, None, msg) for Failed run
        Because cli.py uses this for an exec file.
    """
    validate_setup()
    status = 0  # Bash exit code 0 means OK
    results = Status(Ok, None, None)   # Assume Ok
    try:
        top_level_trainer(argv)
    except speed_exceptions.TrainerOnlyOneCommandPerSessionPermitted as err:
        results = Status(Fail, None, err)
    except Exception as err:
        results = Status(Fail, None, err)
    if results.status == Ok:
        return 0
    else:
        print(f"{results.msg}")
        return 1

if __name__ == '__main__':
    status = main(sys.argv)
    if status == 0:
        exit(0)    # bash uses 0 for Ok return
    else:
        exit(1)    # bash uses non-zero for failed
