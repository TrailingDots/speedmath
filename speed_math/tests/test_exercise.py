#!/bin/env python
"""
Unit testing the speed math project.
"""

import contextlib
import fnmatch
import io
import os
import random
import re
import shutil
import sys
import unittest
from unittest import mock

sys.path.append("./speed_math")

# Set the switch in the constants to use testing.
os.environ['IS_TESTING'] = 'True'
import constants

from speed_math import exercise
from speed_math import *
from speed_math import speed_config
from speed_math.speed_config import SpeedConfig
from speed_math import speed_math
from speed_math.speed_math import Trainer
from speed_math.speed_exceptions import *
from speed_math.speed_exceptions import TrainerConfigFileDoesNotExist
from speed_config import Ok, Fail, Status, Values

constants.print_constants()


class TestBoundsRandomInt(unittest.TestCase):
    """Test the random_bounds() method.
        python  -m unittest speed_math.tests.test_exercise.TestBoundsRandomInt
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    def test_bounds_random_int(self):
        """Simple test for 3 wide values."""

        random_bounds = exercise.Exercise({})

        lower, upper = random_bounds.bounds_random_int(3)
        assert lower == 10**2        # 100
        assert upper == (10**3 - 1)  # 999

        lower, upper = random_bounds.bounds_random_int(1)
        assert lower == 1
        assert upper == 9

        lower, upper = random_bounds.bounds_random_int(8)
        assert lower == 10**7
        assert upper == (10**8 - 1)


class TestProblemValue(unittest.TestCase):
    """Verify that a problem can be run properly.
        python  -m unittest speed_math.tests.test_exercise.TestProblemValue
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    def test_problem_value(self):
        the_exercises = exercise.Exercise({})

        # Get a "random" value. The seed gets reset before
        # calling this test method.
        value = the_exercises.problem_value(3)
        assert value == 543


class TestMultiplication(unittest.TestCase):
    """Tests the multiplication logic.
        python  -m unittest speed_math.tests.test_exercise.TestMultiplication
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_mult(self, mocked_input):
        """Given minimal config structs, test the
        multiplication logic.
        ipython  -m unittest -- speed_math.tests.test_exercise.TestMultiplication.test_mult
        """

        config = {
            'common': {
                "reverse_input": True,
            },
            'multiplication': {
                "multiplicand_wide": 2,  # 3 cols of digits to multiply
                "multiplier_wide": 2,
                "loops": 3  # 3 loops per session
            }
        }
        exercise_mult = exercise.Exercise(config)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        # Because the digits get recorded right to left
        # we get '0062'
        mocked_input.side_effect = ['0062']
        result = exercise_mult.multiplication()
        self.assertTrue(result)

    @mock.patch('builtins.input', create=True)
    def test_mult_choices_of_11_12_and_alternate_default(self, mocked_input):
        """Use a default_config with choices.
        ipython  -m unittest -- speed_math.tests.test_exercise.TestMultiplication.test_mult_choices_of_11_12_and_alternate_default
        """
        # Simulate sys.argv
        user_argv = ['trainer.py',
                '--config', 'mult_11_12_test.yaml',
                '--default_config_file', 'default_test.yaml',
                '--mult']

        # Config from users args
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 165.
        # Because the digits get recorded right to left
        # we get '6090964'. Determined from hacking.
        # Remember that setUp() seeded the random generator.
        mocked_input.side_effect = ['4690906']

        result = atrainer.user_operation(parser)
        # Result consists of a named tuple Status
        # and expect #right to be 1, #wrong to be 0
        self.assertEqual(result.status, Ok)
        self.assertEqual(result.values.right, 1)
        self.assertEqual(result.values.wrong, 0)

    def test_mult_choices_invalid_ints(self):
        """Use a default_config with choices
        ipython  -m unittest -- speed_math.tests.test_exercise.TestMultiplication.test_mult_choices_invalid_ints
        """
        # Simulate sys.argv
        user_argv = ['trainer.py',
                '--config', 'choices_invalid_int_test.yaml',
                '--default_config_file', 'default_test.yaml',
                '--mult']

        # Config from users args
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)

        results = atrainer.user_operation(parser)
        self.assertEqual(results.status, Fail)


class TestAdd(unittest.TestCase):
    """Testing the addition logic.
        ipython  -m unittest -- speed_math.tests.test_exercise.TestAdd
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_add(self, mocked_input):
        config = {
            'common': {
                "reverse_input": True,
            },
            'addition': {
                "wide": 2,  # 2 cols of digits to add
                "rows": 3,  # 3 rows of numbers
                "loops": 3  # 3 loops per session
            }
        }
        exercise_add = exercise.Exercise(config)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        # Because the digits get recorded right to left
        # we get '321'
        mocked_input.side_effect = ['321']
        result = exercise_add.addition()
        self.assertTrue(result)

    @mock.patch('builtins.input', create=True)
    def test_add_non_numeric_entry(self, mocked_input):
        """Non-numeric entries must cause an exception"""

        config = {
            'common': {
                "reverse_input": True,
            },
            'addition': {
                "wide": 2,  # 2 cols of digits to add
                "rows": 3,  # 3 rows of numbers
                "loops": 3  # 3 loops per session
            }
        }
        exercise_add = exercise.Exercise(config)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        # Because the digits get recorded right to left
        # we get '0062'
        mocked_input.side_effect = ['5X1']
        result = exercise_add.addition()
        self.assertFalse(result)


class TestRunAdd(unittest.TestCase):
    """Tests the loop of a session of addition()
    ipython -m unittest -- speed_math.tests.test_exercise.TestRunAdd
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_run_add(self, mocked_input):
        """Test an addition session with reverse, rtl, input.
        """

        config = {
            'common': {
                "reverse_input": True,
            },
            'addition': {
                "wide": 2,  # 2 cols of digits to add
                "rows": 3,  # 3 rows of numbers
                "loops": 3  # 3 loops per session
            }
        }
        exercise_add = exercise.Exercise(config)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        # Because the digits get recorded right-to-left
        # we get '501'. Ignore right/wrong answers.
        mocked_input.side_effect = ['501', '501', '501']
        result = exercise_add.run_addition_problems()
        self.assertTrue(result)

    @mock.patch('builtins.input', create=True)
    def test_add_non_numeric_entry(self, mocked_input):
        """Non-numeric entries must cause an exception"""

        config = {
            'common': {
                "reverse_input": True,
            },
            'addition': {
                "wide": 2,  # 2 cols of digits to add
                "rows": 3,  # 3 rows of numbers
                "loops": 3  # 3 loops per session
            }
        }
        exercise_add = exercise.Exercise(config)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        # Because the digits get recorded right to left
        # we get '0062'
        # Notice the 'X' input char.
        mocked_input.side_effect = ['5X1', '5x1', '5x1']
        result = exercise_add.addition()
        self.assertFalse(result)


class TestAdd_3(unittest.TestCase):
    """Test the add_3.yaml config file.
       ipython -m unittest -- speed_math.tests.test_exercise.TestAdd_3
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_run_add_3(self, mocked_input):
        """Test that config files get loaded properly
        at the top level.
        ipython  -m unittest -- speed_math.tests.test_exercise.TestAdd_3.test_run_add_3
        """

        argv_str = "test_exercise.py --config add_3.yaml --addition"
        user_argv = argv_str.split()

        # mocked "answers" are reversed.
        mocked_input.side_effect = ['884', '864', '757']

        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)
        status = atrainer.user_operation(parser)
        self.assertEqual(status.status, Ok)
        self.assertEqual(status.values.right, 3)
        self.assertEqual(status.values.wrong, 0)

    def test_multiplication_commands_do_not_work(self):
        """
        Cannot run multiplication commands from CLI
        python  -m unittest speed_math.tests.test_exercise.TestAdd_3.test_multiplication_commands_do_not_work
        """
        argv_str = "test_exercise.py --config add_3.yaml --addition --multiplication"
        user_argv = argv_str.split()
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)
        self.assertRaises(Exception, lambda: atrainer.user_operation(parser))

    def test_no_commands_do_not_work(self):
        """
        Must have at least one command from CLI
        python  -m unittest speed_math.tests.test_exercise.TestAdd_3.test_no_commands_do_not_work
        """
        argv_str = "test_exercise.py --config add_3.yaml"
        user_argv = argv_str.split()
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)
        self.assertRaises(Exception, lambda: atrainer.user_operation(parser))

    @mock.patch('builtins.input', create=True)
    def test_speed_math(self, mocked_input):
        """Run a session from the top.
        ipython  -m unittest -- speed_math.tests.test_exercise.TestAdd_3.test_speed_math
        """

        mocked_input.side_effect = ['884', '864', '757']

        argv_str = "test_exercise.py --config add_3.yaml --addition"
        user_argv = argv_str.split()
        status = speed_math.top_level_trainer(user_argv)
        self.assertEqual(status.values.right, 3)
        self.assertEqual(status.values.wrong, 0)

    @mock.patch('builtins.input', create=True)
    def test_speed_math_no_user_answer(self, mocked_input):
        """Run a test with the user just entering
        a return instead of a value.
        ipython  -m unittest -- speed_math.tests.test_exercise.TestAdd_3.test_speed_math_no_user_answer
        """

        argv_str = "test_exercise.py --config add_3.yaml --addition"
        mocked_input.side_effect = ['', '864', '757']
        user_argv = argv_str.split()
        status = speed_math.top_level_trainer(user_argv)
        self.assertEqual(status.values.right, 2)
        self.assertEqual(status.values.wrong, 1)


class TestMissingConfig(unittest.TestCase):
    """Test for config file not provided by user.
        ipython -m unittest -- speed_math.tests.test_exercise.TestMissingConfig
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_no_config(self, mocked_input):
        """Command line contains no --config so use default_config.
        The default configures reversed, right-to-left input."""

        argv_str = "test_exercise.py --addition"

        # mocked "answers" are reversed.
        # Values discovered by running the test and providing
        # the displayed values.
        mocked_input.side_effect = [
                '900907'[::-1], '880813'[::-1], '771312'[::-1],
                '878674'[::-1], '883178'[::-1], '327945'[::-1],
                '1393810'[::-1], '1081414'[::-1], '1328472'[::-1],
                '981075'[::-1]]
        user_argv = argv_str.split()
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)
        result = atrainer.user_operation(parser)
        num_right, num_wrong, time_delta = result
        self.assertEqual(result.values.right, 10) # number success entries
        self.assertEqual(result.values.wrong, 0)  # number incorrect answers


class TestInvalidConfig(unittest.TestCase):
    """Test for invalid config conditions.
        ipython -m unittest -- speed_math.tests.test_exercise.TestInvalidConfig
    """

    # Need to investigate. The exception DOES work and gets printed.
    def test_invalid_conf_filename(self):
        # Simulate sys.argv
        argv_str = "test_exercise.py --config mult_11_12_test.yaml " + \
                "--default_config_file XXX.yaml " + \
                "--mult"
        user_argv = argv_str.split()
        user_argv_dict, parser = speed_math.build_user_arg_dict(user_argv)
        with self.assertRaises(Exception) as _err:
            speed_config.SpeedConfig(user_argv_dict, parser)


class TestValidateSetup(unittest.TestCase):
    """The validate_setup should test for a working software
    config.
    How do I "uninstall" deepmerge for this test?
    I'll just ignore it for coverage purposes.
    """
    def test_validate(self):
        result = speed_math.validate_setup()
        self.assertTrue(result)


class TestListConfigs(unittest.TestCase):
    """Test the list_configs option.
        ipython -m unittest -- speed_math.tests.test_exercise.TestListConfigs
    """

    def test_simple_config_list_request(self):
        argv_str = "test_exercise.py -l"
        user_argv = argv_str.split()

        string_io = io.StringIO()
        with contextlib.redirect_stdout(string_io):
            (user_arg_dict, parser) = speed_math.build_user_arg_dict(user_argv)
            SpeedConfig(user_arg_dict, parser)

            self.assertTrue(user_arg_dict['list_configs'])

        output = string_io.getvalue()

        # Examine the output for SYNTAX error in 2 yaml files.
        # Two yaml files have deliberate errors.
        bad = re.findall("add_bad.yaml\n    SYNTAX ERROR", output)
        assert len(bad) == 1
        bad2 = re.findall("add_bad2.yaml\n    SYNTAX ERROR", output)
        assert len(bad2) == 1

        # Only two errors in the test yaml files
        bad = re.findall("SYNTAX ERROR", output)
        assert len(bad) == 2


class TestYAMLLoading(unittest.TestCase):
    """Test proper error handling for yaml loading
        ipython -m unittest -- speed_math.tests.test_exercise.TestYAMLLoading
    """

    def test_bad_yaml_label(self):
        """Properly detects YAML syntax error.
        python -m unittest speed_math.tests.test_exercise.TestYAMLLoading.test_bad_yaml_label
        """

        argv_str = "test_exercise.py -c add_bad2.yaml -a"
        user_argv = argv_str.split()

        (user_arg_dict, parser) = speed_math.build_user_arg_dict(user_argv)

        self.assertRaises(Exception, 
                SpeedConfig, user_arg_dict, parser)


class TestDivision(unittest.TestCase):
    """Test the random_bounds() method.
        ipython  -m unittest -- speed_math.tests.test_exercise.TestBoundsRandomInt
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_simple_division_problem(self, mocked_input):
        """Test formatting of division problem.
        ipython -m unittest -- speed_math.tests.test_exercise.TestDivision.test_simple_division_problem
        """
        config = {
            'title': "3 digits dividend, 1 divisor. 3 loops",
            'common': {
                "reverse_input": False
            },
            'division': {
                "dividend_wide": 3,  # 3 cols of digits to divide
                "divisor_wide": 1,
                "loops": 3  # 3 loops per session
            }
        }
        exercise_div = exercise.Exercise(config)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        mocked_input.side_effect = ['109', '123', '456']
        result = exercise_div.division()
        self.assertTrue(result)


class TestMain(unittest.TestCase):
    """Test the main() fcn
    ipython -m unittest -- speed_math.tests.test_exercise.TestMain
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_run_main(self, mocked_input):
        """Test that main(argv) follows the top
        level logic properly.
        ipython -m unittest -- speed_math.tests.test_exercise.TestMain.test_run_main
        """

        argv_str = "test_exercise.py --config add_3.yaml --addition"
        argv = argv_str.split(' ')

        # mocked "answers" are reversed.
        mocked_input.side_effect = ['884', '864', '757']

        result = speed_math.main(argv)
        self.assertTrue(result == 0)

    @mock.patch('builtins.input', create=True)
    def test_run_only_one_command(self, mocked_input):
        """Test that main(argv) catches
        TrainerOnlyOneCommandPerSessionPermitted 
        properly.
        ipython -m unittest -- speed_math.tests.test_exercise.TestMain.test_run_only_one_command
        """

        argv_str = \
            "test_exercise.py --config add_3.yaml --addition --division"
        argv = argv_str.split(' ')

        # mocked "answers" are reversed.
        mocked_input.side_effect = ['884', '864', '757']

        # main() returns 0 or 1, NOT a tuple!
        results = speed_math.main(argv)
        self.assertTrue(results, 1)


class TestDivisionChoices(unittest.TestCase):
    """Use choices in division problems.
    ipython -m unittest -- speed_math.tests.test_exercise.TestDivisionChoices
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_format_division_choices(self, mocked_input):
        # Simulate sys.args
        user_argv = ["trainer.py",
                "--config", "div_11_12.yaml",
                "--default_config_file", "default_test.yaml",
                "--division"]
        # Config from users args
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        mocked_input.side_effect = ['39', '14', '30']

        result = atrainer.user_operation(parser)

        # Result consists of a tuple: (#correct, #wrong, time)
        # and expect #correct to be 1, #wrong to be 0
        self.assertTrue(result)

    def test_format_division_bad_choices(self):
        """
        ipython -m unittest -- speed_math.tests.test_exercise.TestDivisionChoices.test_format_division_bad_choices
        """
        # Simulate sys.args
        user_argv = ["trainer.py",
                "--config", "div_11_12_bad_choice.yaml",
                "--default_config_file", "default_test.yaml",
                "--division"]
        # Config from users args
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)

        results = atrainer.user_operation(parser)
        self.assertEqual(results.status, Fail)

    @mock.patch('builtins.input', create=True)
    def test_format_division_bad_dividend(self, mocked_input):
        """
        ipython  -m unittest -- speed_math.tests.test_exercise.TestDivisionChoices.test_format_division_bad_dividend
        """
        # Simulate sys.args
        user_argv = ["trainer.py",
                "--config", "div_11_12_bad_dividend.yaml",
                "--default_config_file", "default_test.yaml",
                "--division"]
        # Config from users args
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the result should be 2600.
        mocked_input.side_effect = ['39', '14', '30']

        results = atrainer.user_operation(parser)
        self.assertEqual(results.status, Fail)

    @mock.patch('builtins.input', create=True)
    def test_division_choices(self, mocked_input):
        """
        python -m unittest speed_math.tests.test_exercise.TestDivisionChoices.test_division_choices
        ipython -m unittest -- speed_math.tests.test_exercise.TestDivisionChoices.test_division_choices
        """
        # Simulate sys.args
        user_argv = ["trainer.py",
                "--config", "div_by_choices_67.yaml",
                "--default_config_file", "default_test.yaml",
                "--division"]

        # Config from users args
        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)

        # Because mocking gets used for 'random' numbers,
        # tracing reveals the results.
        # Division digits operate left to right.
        mocked_input.side_effect = ['78', '28', '47']

        results = atrainer.user_operation(parser)
        self.assertEqual(results.values.right, 3)  # Correct answers
        self.assertEqual(results.values.wrong, 0)  # Wrong answers


class TestSub_3(unittest.TestCase):
    """Test the sub_3.yaml config file.
    ipython -m unittest -- speed_math.tests.test_exercise.TestSub_3
    """

    def setUp(self):
        """ To get repeatable results, seed the
        random number generator before each test.
        """
        random.seed(1234567)

    @mock.patch('builtins.input', create=True)
    def test_run_sub_3(self, mocked_input):
        """Test that config files get loaded properly
        at the top level.
        ipython -m unittest -- speed_math.tests.test_exercise.TestSub_3.test_run_sub_3
        """

        argv_str = "test_exercise.py --config sub_3.yaml --subtraction"
        user_argv = argv_str.split()

        # mocked "answers" are reversed.
        mocked_input.side_effect = ['47366', '09181', '56315']

        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)
        status = atrainer.user_operation(parser)
        self.assertEqual(status.status, Ok)
        self.assertEqual(status.values.right, 3)
        self.assertEqual(status.values.wrong, 0)


    @mock.patch('builtins.input', create=True)
    def test_run_sub_5_choices(self, mocked_input):
        """Sub 5 digit numbers with choices.
        ipython -m unittest -- speed_math.tests.test_exercise.TestSub_3.test_run_sub_5_choices
        """

        argv_str = "test_exercise.py --config sub_5_choices.yaml --subtraction"
        user_argv = argv_str.split()

        # mocked "answers" are reversed.
        mocked_input.side_effect = ['60766', '67971', '70793']

        user_arg_dict, parser = speed_math.build_user_arg_dict(user_argv)
        atrainer = Trainer(user_arg_dict, parser)
        status = atrainer.user_operation(parser)
        self.assertEqual(status.status, Ok)
        self.assertEqual(status.values.right, 3)
        self.assertEqual(status.values.wrong, 0)


if __name__ == "__main__":
    unittest.main(verbosity=2)

