#!/bin/env python3
"""A class that loads a configuration file
and runs the requested exercises."""

import math
import random
import sys
import time

from speed_config import Ok, Fail, Status, Values

class Exercise():
    """
    Given a config file as described by a user provided yaml
    file, run an exercise.

    Each operation requires different parameters from the
    config file.
    """
    def __init__(self, config):
        self.config = config

        # Operators set this depending on config file
        self.choices = None

    #======================================================
    # Routines common to all integers
    #======================================================

    def do_int_input(self, msg):
        """Input the user"s answer to the problem.  Convert
        the answer to a value.  The answer string may be
        reversed if that was that the user"s request in the
        config file.

        Returns:
            A Status namedtuple.
                status = Ok or Fail
                values = Converted integer
                msg = None or msg for conversion error.

            Status(Ok, int_value, None)
            Status(Fail, None, error_msg)
        """

        # The user enters an answer.
        # All input must consist of digits only.
        str_answer = input(msg)

        # The user may enter rtl or ltr digits. This gets
        # configured in the config file.
        # If the user wants right-to-left input numbers
        if self.config["common"]["reverse_input"]:
            # User input requests right-to-left, so reverse.
            str_answer = str_answer[::-1]
            print(f"\nAnswer from reversed string: {str_answer}")

        # Users may enter non-digits and this results in a
        # False return value.
        try:
            user_answer = int(str_answer)
        except ValueError as err:
            # User has entered a non-digit character.
            # Print an error msg and ignore this problem.
            error_msg = f"{str_answer}: {err.__doc__}"
            print(error_msg)
            return Status(Fail, None, error_msg)

        return Status(Ok, user_answer, None)

    def bounds_random_int(self, digits_in_column):
        """
        Given the number of digits in each column, return
        the lower and upper bounds of the problem.

        digits_in_column - The number of digits per column.

        Returns a tuple:
            lower - The lower value for a random number generator.
            upper - The upper value for a random number generator.

        As an example:
            lower, upper = digits_in_column(3)
            lower -> 10**2       == 100
            upper -> (10**3 - 1) == 999
        Meaning the values have a range of 100 to 999
        inclusive.
        """
        upper = int(math.pow(10, digits_in_column) - 1)
        lower = int(math.pow(10, digits_in_column - 1))
        return lower, upper

    def problem_value(self, digits_in_column):
        """Given the number of digits per column, return a
        random integer with this number of digits.

        Returns:
            Status(Ok, converted_int, None)
            Status(Fail, None, error_msg)

        """
        # For single digit values, do not use 0 or 1
        if digits_in_column == 1:
            valid_numerals = list(range(2, 10))
            return random.choice(valid_numerals)
        lower, upper = self.bounds_random_int(digits_in_column)
        return int(random.randrange(lower, upper))

    def compare_user_to_actual_answer(self, user, actual):
        """
        Compare user answer vs actual and provide a
        display comment.
        Output the congratulatory response or a blah.
        """
        # Output alignment has been adjusted for easy
        # reading.
        if user == actual:
            print(f"Great -     expected answer: {actual}\n")
            return True
        else:
            print(f"Sorry -     expected answer: {actual}\n")
            return False

    def run_operator_common(self, loops, operator, operator_name):
        """
        Run a session of the operator.
        This routine contains all the common
        code between these operators.

        Returns a named tuple of Status with Values:
            Status(Ok, 
                   Values(num_right, num_wrong, time_delta),
                   None)
        """

        # Configure the choices, if any.
        choice = self.process_choices(operator_name)
        if choice.status == Fail:
            return choice

        num_right = 0
        num_wrong = 0

        # Start a timer for the problem set.
        time_start = time.time()

        # Emphasize the ltr or rtl digits entry.
        if self.config["common"]["reverse_input"]:
            print("\n\t****************************************")
            print("\t* Enter right-to-left/REVERSED digits! *")
            print("\t****************************************\n\n")
        else:
            print("\n\t*******************************")
            print("\t* Enter left-to-right digits! *")
            print("\t*******************************\n\n")

        for _ in range(loops):
            result = operator()
            if result:
                num_right += 1
            else:
                num_wrong += 1

        # End the timer for the problem set.
        # Get the time delta.
        time_delta = time.time() - time_start

        print(f"Right answers = {num_right}")
        print(f"Wrong answers = {num_wrong}")
        print(f"Session time  = {time_delta:6.2f} sec")

        return Status(Ok,
                      Values(right=num_right,
                             wrong=num_wrong,
                             timing=time_delta),
                      None)


    def process_choices(self, operation_name):
        """
        Sometimes a user wants only one, two or 
        a few specific values. The user provides 
        a comma separated list of values for the
        operation values. The value displayed will
        be a random choice within these values.

        For example, they want to practice on both 11 and
        12 and no others. In the yaml config file, use:
            config: 11,12

        operation_name is the name of the operation.
        This is either multiplication, addition,
        subtraction, or division.

        Returns:
            A Status namedtuple.
                status = Ok or Fail
                values = converted integer
                msg    = None or msg for conversion error.

            Status(Ok, None, None) = No user supplied choices
            Status(Ok, choice_list, None) = List of user choices
            Status(Fail, None, error_msg) = Syntex error in integers

        Notice that exceptions are not raised but contained
        in this method! This makes testing much easier.
        """

        choices_str = self.config[operation_name].get("choices", False)
        if not choices_str:
            # Config file has no "choice", just return
            return Status(Ok, None, None)
        try:
            self.choices = [int(achoice) for achoice in choices_str.split(",")]
        except ValueError as err:
            # Return a tuple instead of exception.
            err_str = " ".join([
                    str(err),
                    "\n",
                    f"Invalid integers in config file choices list"])
            return Status(Fail, None,
                f"Invalid integers in config file choices list: {choices_str}")

        # Valid choices
        return Status(True, self.choices, None)

    #============================================================
    # Routines common to multiplication
    #============================================================

    def format_multiplication_problem(self, multiplicand, multiplier):
        """
        Returns a reasonable display string for multiplication.

        This routine solves the alignment problem when
        displaying multiplication problems vertically.
        Both numbers must be vertically aligned to the
        right.

        Depending on the config settings, create a usable
        display.

        Returns:
            A string of the problem display.

        TODO: Optionally display horizontally rather than vertically?
        """
        multiplication = self.config["multiplication"]
        digits_in_multiplicand = multiplication["multiplicand_wide"]

        # If a "choices" keyword, the multiplier needs to
        # determine the multiplier_wide setting.
        if not self.choices:
            # No choice, user multiplier_wide setting
            digits_in_multiplier = multiplication["multiplier_wide"]
        else:
            # Choice exists. Find out how wide for the multiplier
            digits_in_multiplier = int(math.log10(max(self.choices))) + 1

        max_width = max(digits_in_multiplicand, digits_in_multiplier)
        multiplicand_str = f"{multiplicand}"
        # Assume the # digits in multiplier is <= # digits in multiplicand
        multiplier_str = f"{multiplier}"
        num_spaces = max_width - len(multiplier_str)
        problem_string = (f"\t {multiplicand_str}\n"
                          f"\t*{' '*num_spaces}{multiplier_str}\n"
                          f"\t{'-'*(max_width+1)}\n\t")
        return problem_string

    def multiplication(self):
        """
        Create and display a multiplication problem
        """

        multiply = self.config["multiplication"]
        digits_in_multiplicand = multiply["multiplicand_wide"]
        number1 = self.problem_value(digits_in_multiplicand)

        digits_in_multiplier = multiply["multiplier_wide"]

        if not self.choices:
            number2 = self.problem_value(digits_in_multiplier)
        else:
            # The user has requested a specific list of multipliers.
            number2 = random.choice(self.choices)

        # Format the numbers into the appropriate sized strings.
        problem_string = self.format_multiplication_problem(number1, number2)

        # Display problem and solicit answer.
        # Need a more general formatter for a problem!
        user_answer = self.do_int_input(problem_string)

        if user_answer.status == Fail:
            return False

        real_answer = number1 * number2
        return self.compare_user_to_actual_answer(user_answer.values,
                                                  real_answer)

    def run_multiplication_problems(self):
        """
        Present and grade multiplication problems.

        Return a Status namedtuple
        """

        # The default config should have loops, but we
        # do this for robustness.
        # Hard coded loop default - not good but reasonable.
        loops = self.config["multiplication"].get("loops", 10)

        results = self.run_operator_common(loops, 
                    self.multiplication,
                    "multiplication")
        return results


    def addition(self):
        """
        Present and grade a single addition problem
        """

        digits_in_column = self.config["addition"]["wide"]
        numbers = [self.problem_value(digits_in_column)
                   for num in range(self.config["addition"]["rows"])]
        for num in numbers:
            print(f"\t {num}")
        print(f"\t {'-'*digits_in_column}")

        user_answer = self.do_int_input("\t")

        if user_answer.status == Fail:
            return False

        real_answer = sum(numbers)
        return self.compare_user_to_actual_answer(user_answer.values,
                                                  real_answer)

    def run_addition_problems(self):
        """
        Present and grade addition problems.

        Returns a named tuple of Status with Values:
            Status(Ok, 
                   Values(num_right, num_wrong, time_delta),
                   None)
        """
        # The default config should have this value, but we
        # do this for robustness.
        # Hard coded loop default - not good but reasonable.
        loops = self.config["addition"].get("loops", 10)

        return self.run_operator_common(loops, self.addition, "addition")

    def run_subtraction_problems(self):
        """
        Present and grade subtraction problems.

        Returns a named tuple of Status with Values:
            Status(Ok, 
                   Values(num_right, num_wrong, time_delta),
                   None)
        """
        # The default config should have this value, but we
        # do this for robustness.
        # Hard coded loop default - not good but reasonable.
        loops = self.config["subtraction"].get("loops", 10)

        return self.run_operator_common(loops, self.subtraction, "subtraction")

    def subtraction(self):
        """
        Create and display a subtraction problem.
        """

        subtract = self.config["subtraction"]
        digits_in_minuend = subtract["minuend_wide"]
        number1 = self.problem_value(digits_in_minuend)

        digits_in_subtrahend = subtract["subtrahend_wide"]

        if not self.choices:
            number2 = self.problem_value(digits_in_subtrahend)
        else:
            # The user has requested a specific list of multipliers.
            number2 = random.choice(self.choices)

        # Format the numbers into the appropriate sized strings.
        problem_string = self.format_subtraction_problem(number1, number2)

        # Display problem and solicit answer.
        # Need a more general formatter for a problem!
        user_answer = self.do_int_input(problem_string)

        if user_answer.status == Fail:
            return False

        real_answer = number1 - number2
        return self.compare_user_to_actual_answer(user_answer.values,
                                                  real_answer)


    def format_subtraction_problem(self, minuend, subtrahend):
        """
        Reasonably provide a formating string for
        subtraction.

        This routine solves the alignment problem when
        displaying subtraction problems vertically.
        Both numbers must be vertically aligned to the
        right.

        Depending on the config settings, create a usable
        display.

        TODO: Optionally display horizontally rather than vertically?
        """
        subtract = self.config["subtraction"]
        digits_in_minuend = subtract["minuend_wide"]

        # If a "choices" keyword, the multiplier needs to
        # determine the multiplier_wide setting.
        if not self.choices:
            # No choice, user multiplier_wide setting
            digits_in_subtrahend = subtract["subtrahend_wide"]
        else:
            # Choice exists. Find out how wide for the multiplier
            digits_in_subtrahend = int(math.log10(max(self.choices))) + 1

        max_width = max(digits_in_minuend, digits_in_subtrahend)
        minuend_str = f"{minuend}"

        # Assume the # digits in subtrahend is <= # digits in minuend
        subtrahend_str = f"{subtrahend}"
        num_spaces = max_width - len(subtrahend_str)
        problem_string = (f"\t {minuend_str}\n"
                          f"\t {' '*(num_spaces-1)}-{subtrahend_str}\n"
                          f"\t{'-'*(max_width+1)}\n\t")
        #print(f"problem string: {problem_string}")
        return problem_string


    #============================================================
    # Routines common to division   dividend / divisor
    #============================================================


    def format_division_problem(self, dividend, divisor):
        """
        Provide a reasonable formating string for
        division.

        This routine solves the alignment problem when
        displaying division problems vertically.
        Both numbers must be vertically aligned to the
        right.

        Depending on the config settings, create a usable
        display.
        """
        division = self.config["division"]
        digits_in_dividend = division["dividend_wide"]

        # If a "choices" keyword, the division needs to
        # determine the divisor_wide setting.
        if not self.choices:
            # No choice, user divisor_wide setting
            digits_in_divisor = division["divisor_wide"]
        else:
            # Choice exists. Find out how wide for the multiplier
            digits_in_divisor = int(math.log10(max(self.choices))) + 1

        # TODO  - these str are not needed. Remove
        dividend_str = f"{dividend}"
        # Assume the # digits in divisor is <= # digits in dividend
        divisor_str = f"{divisor}"
        problem_string = (f"\t {dividend_str} / "
                          f"{divisor_str} = ")
        return problem_string

    def division(self):
        """
        Create and display a division problem
        """

        division = self.config["division"]
        digits_in_dividend = division["dividend_wide"]
        dividend = self.problem_value(digits_in_dividend)

        digits_in_divisor = division["divisor_wide"]

        if not self.choices:
            divisor = self.problem_value(digits_in_divisor)
        else:
            # The user has requested a specific list of multipliers.
            divisor = random.choice(self.choices)

        # Force division answer to be a round integer with no left overs
        rounded = round(dividend/divisor)
        adjusted_dividend = rounded * divisor
        real_answer = int(adjusted_dividend/divisor)

        # Format the numbers into the appropriate sized strings.
        problem_string = self.format_division_problem(adjusted_dividend, divisor)

        # Division is ALWAYS left to right.
        self.config["common"]["reverse_input"] = False

        # Display problem and solicit answer.
        user_answer = self.do_int_input(problem_string)
        if user_answer.status == Fail:
            return False

        return self.compare_user_to_actual_answer(user_answer.values,
                                                  real_answer)


    def run_division_problems(self):
        """
        Present and grade division problems.

        Returns a named tuple of Status with Values:
            Status(Ok, 
                   Values(num_right, num_wrong, time_delta),
                   None)
        """
        # The default config should have loops, but we
        # do this for robustness.
        # Hard coded loop default - reasonable?
        loops = self.config["division"].get("loops", 10)

        stat = self.run_operator_common(loops, self.division, "division")
        return stat

