"""
All exceptions for the trainer code.
"""

# All caught exceptions in this package derive from
# TrainerException
class TrainerException(Exception):
    """Base class for all exceptions for this module"""
    pass


class TrainerNoCommandInCommandLine(TrainerException):
    """The command line has not provided a math operation argument.
    Examples:
        python speed_math/speed_math.py --add -c add_3.yaml
    """
    pass


class TrainerOnlyOneCommandPerSessionPermitted(TrainerException):
    """The command line must contain only one command per session."""
    pass


class TrainerInvalidChoicesList(TrainerException):
    """The user has provided a non-numeric in the choice list."""
    pass


class TrainerConfigFileDoesNotExist(TrainerException):
    """Config file does not exists"""
    pass

