"""
    The speed_config module contains code for
    configuring the project.

    Based on the command line arguments, this code reads the
    user and default config files.

    A runtime config gets created by starting with a default
    config and applying the user config parameters to update
    the default. User settings override default settings.
"""

from collections import namedtuple
import copy
import os
import os.path
import sys

import deepmerge

import yaml     # pyyaml

from speed_exceptions import TrainerConfigFileDoesNotExist

# Where is the speed trainer config file?
# Context() uses a singleton to hold run-time settings.
import constants

#============================================================
# Uses namedtuple from "Write Unbreakable Python"
# https://jessewarden.com/2020/03/write-unbreakable-python.html
#============================================================
Ok = "Ok"
Fail = "Fail"


# Used to encapsulate the return status, the method/fcn
# values and a possible error message.
# Use the msg when status != Ok
#
# status = Ok or Fail
# values = A list of values returned from a method
# msg    = None or an error message
Status = namedtuple("Status", "status values msg")

# Values contains the simple triple of values from
# a problem. right and wrong are int counts. timing
# has the timing in seconds
Values = namedtuple("Values", "right wrong timing")


class SpeedConfig():
    """Configuration file processing trainer.  """

    def __init__(self, user_args, parser):
        """
        user_args: The dict of options and values
        parser   : The command line parser
        """
        self.common = {}
        self.add = {}
        self.multiply = {}
        self.speed_config = {}   # Configuration maps from user config file
        self.user_args = user_args  # user args dict from the command line.
        self.parser = parser

        # Merge the default config into the user configs.
        # User config will override default configs.
        self.default_config_dir = constants.SPEED_MATH_CONFIG_DIR

        # The user may have supplied a default file. Use that
        # if present.
        # May be modified by load_config_files()
        self.user_config_dir = constants.SPEED_MATH_CONFIG_DIR

        # Perform a listing of config titles if requested.
        if self.user_args.get('list_configs', False):
            self.list_configs()

        # Set the user config to the requested value.
        # If the config file fails to load, an exception
        # will be thrown.
        self.config = self.load_config_files()

    def _deep_merge_config(self, default, user):
        """Performs a deep merge of the default and user
        config.

        Returns merged dictionary.

        Ref: https://deepmerge.readthedocs.io/en/latest/
        """

        # Destructive merge, so needs a deepcopy
        default_deep = copy.deepcopy(default)

        # Configure the merger.
        my_merger = deepmerge.Merger([(list, ["append"]),
                                      (dict, ["merge"])
                                     ],
                                     ["override"],
                                     ["override"])

        # Perform the deep merge.
        my_merger.merge(default_deep, user)

        # default_deep has been destructively updated.
        # That's OK since default_deep has been copied.
        return default_deep

    def find_config_file(self, config_filename):
        """Find the config file.
        This logic works for default and user config filenames.

        Verify existence of config_filename. If found, return that.

        Otherwise look in the ~/.speed_math/ dir.

        Otherwise, error out of program.

        Return:
            The fully qualified path of the config file.
        """

        # First, get the default config dir setup.
        abs_config_dir = constants.SPEED_MATH_CONFIG_DIR

        # Look in default_config_filename
        # If we can access the config file directly, do it
        if os.path.isfile(config_filename):
            return config_filename

        # Using the default config dir, find the config.
        # If this is not an absolute config filename,
        # prefix with various dirs
        # Need to convert default_test.yaml into an absolute name.
        abs_user_config_file = abs_config_dir + config_filename
        if os.path.isfile(abs_user_config_file):
            return abs_user_config_file

        err = (
            f"\nConfig filename does not exist: "
            f"{abs_user_config_file} "
            f"\n\tCurrent dir: {os.getcwd()}")
        raise Exception(''.join([err, constants.constants_str()]))

    def decode_yaml_err(self, yaml_err):
        """Given a yaml error, decode the reason and
        return a string highlighting the problem.
        """
        problem = yaml_err.problem
        mark = yaml_err.context_mark
        line = mark.line
        column = mark.column
        return f"SYNTAX ERROR: {problem}. line {line}. column {column}\n"

    def load_yaml_config_file(self, config_filename):
        """Open config_filename and load into a mapping of
        the yaml data.

        Returns:
            (True, the config from the specified filename)
            (False, the error string from a syntax error in the yaml file)
        """
        try:
            with open(config_filename) as file_handle:
                config = \
                    yaml.load(file_handle, Loader=yaml.SafeLoader)
            return (True, config)
        except yaml.scanner.ScannerError as err:
            return (False, self.decode_yaml_err(err))

    def find_default_config(self):
        """Find the default config file and return the
        config for that file.

        Returns the default configuration."""

        default_config_file = self.user_args['default_config_file']

        # This will bomb if no config file found.
        try:
            # This will raise an error if the default_config_file
            # cannot be found.
            default_config_file = \
                self.find_config_file(default_config_file)
            (status, yaml_config_or_err) = \
                self.load_yaml_config_file(default_config_file)
            if status:
                self.default_config = yaml_config_or_err
                return self.default_config
            raise Exception(f"Default config file {default_config_file} cannot be found")
        except TrainerConfigFileDoesNotExist as err:
            print(err)
            raise Exception("User defined config file cannot be found")

    def find_user_config(self):
        """Find the user specified config and
        return the full user configuration."""
        user_config_file = self.user_args['config']
        if user_config_file is None:
            user_config_file = constants.DEFAULT_CONFIG_FILE
        user_config_file = self.find_config_file(user_config_file)
        if not user_config_file:
            user_config_file = self.default_config_file

        if os.path.exists(user_config_file):
            print(f"User config file: {user_config_file}")
            (status, yaml_config_or_err) = \
                    self.load_yaml_config_file(user_config_file)
            if status:
                self.user_config = yaml_config_or_err
                # Return user configuration from YAML config
                return self.user_config

            # Report as invalid syntax in YAML config
            #TODO: raise TrainerInvalidYAMLSyntax(
            raise Exception(
                f"Invalid YAML syntax in {user_config_file}")

        # Cannot find user config
        raise TrainerConfigFileDoesNotExist(
            f"User config file \"{user_config_file}\" does not exist")

    def load_config_files(self):
        """Load the default and user config from the command
        line args.  Returns the merge of default and user
        config.

        Config files get stored in the speed_math/config dir.
        The user may specify another directory for the config
        setting.

        The self.user_args looks like:
            self.user_args = {'config': 'add_3.yaml', 'add': True,
                    'default_config_file': 'default.yaml',
                    'multiply': False}

        If an error is detected or one of the config file is not
        found, then exit.

        Returns the merged config of the defaults and the user config.
        """
        self.default_config = \
            self.find_default_config()

        self.user_config = \
            self.find_user_config()

        # Merge/Override with defaults. The deep_mege
        # munges the default_config.
        # default_config is a map of the config.
        self.default_config = \
            self._deep_merge_config(self.default_config,
                                    self.user_config)
        return self.default_config

    def list_configs(self):
        """List the titles in each config file.
        Executes only on a -l or --list_config flag.
        Returns None
        """
        config_dir = constants.SPEED_MATH_CONFIG_DIR

        # Assume no subdirs in config directory.
        files = os.listdir(config_dir)

        print(f"\nConfig directory: {config_dir}\n")
        for config_filename in files:
            # Some files in the config dir may not end
            # with yaml, so filter.
            if not config_filename.endswith('.yaml'):
                continue
            print(f"{config_filename}")

            # Load the config and print the title
            the_config = config_dir + config_filename
            (status, yaml_config_or_err) = \
                self.load_yaml_config_file(the_config)
            if not status:
                print(f"    {yaml_config_or_err}")
                continue
            aconfig = yaml_config_or_err
            if aconfig:
                title = aconfig.get('title', False)
                if title:
                    print(f"    {title}\n")
                else:
                    print(f"\tNo title in config {config_filename}\n")
            else:
                # Could not load yaml
                print(f"    ERROR: Could not load {config_filename}\n")

