#!/bin/env python3
#
# Command line interface to speed_math trainings.
#

import sys

import speed_math

exit(speed_math.main(sys.argv))
