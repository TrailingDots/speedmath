#!/bin/bash
#
# Get a word count of source files.
#
echo =========== All files ==============
/usr/bin/wc `find . -name '*.py'   | grep -v htmlcov` \
            `find . -name '*.md'   | grep -v htmlcov` \
            `find . -name '*.sh'   | grep -v htmlcov`
echo
echo ========== Python files only ==============
/usr/bin/wc `find . -name '*.py'` 
