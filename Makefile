DESTDIR=/
PROJECT=speed_math
BUILDDIR=$(CURDIR)/$(PROJECT)
LIBDIR=$(CURDIR)/$(PROJECT)
TOOLSDIR=$(CURDIR)/$(PROJECT)
LOCALDIR=$(HOME)/.local

PYTHON=`which python`

# Coverage requires special handling
COVERAGE=`which coverage`

RM=/usr/bin/rm

RM=/usr/bin/rm
CP=/usr/bin/cp

all:
	make help

help:
	@echo "make help     - This help message."
	@echo "make sdist    - Build source distributable package."
	@echo "make test     - Run the unit tests."
	@echo "make coverage - Run test suite. Capture with 'script' command."
	@echo "make flake8   - Run flake8 with flags"
	@echo "make install  - install on local system"
	@echo "make backup   - Create tgz backup one dir above this dir."
	@echo "make wc       - Perform word count for line counts."
	@echo "make clean    - Get rid of scratch files"
	@echo "make doc      - Create html file from README.md"
	@echo "make lint     - Perform multiple linters with prospector and radon"

sdist:
	rm -rf dist build
	DISTUTILS_DEBUG=1 python setup.py sdist

flake8:
	flake8 --ignore=E128,W391,E126,E121,W291

test:
	$(PYTHON) -m unittest -vvv

coverage:
	$(COVERAGE) run --branch -m unittest -vvv
	$(COVERAGE) report
	$(COVERAGE) html
	@echo
	@echo ============================================================
	@echo To view coverage load: file://$(PWD)/htmlcov/index.html
	@echo ============================================================

install:
	$(PYTHON) setup.py install

wc:
	./tools/wc.sh

backup:
	./tools/backup.sh

# Users may not have all these nagging linters, but that's OK.
lint:
	@echo
	@echo "==== radon: A package of nagging linters ==="
	@echo "======== cc: Cyclomatic Complexity ============"
	@echo "     1-5  A: simple block"
	@echo "     6-10 B: low - well structured and stable block"
	@echo "    11-20 C: moderate - slightly complex block"
	@echo "    21-30 D: More than moderate - more complex block"
	@echo "    31+   F: Very high - error prone, unstable"
	@echo "    Example:"
	@echo "    M 341:4 TestAdd_3.test_speed_math - A (3)"
	@echo " A method test_speed_math in class TestAdd_3"
	@echo " starting at line 341 has complexity of 3."
	radon cc $$(find . -name '*.py') --total-average --show-complexity
	@echo
	@echo ======== mi: Maintainability Index ============
	@echo " The higher the value, the easier to maintain"
	@echo "     9-0  C Extremely low"
	@echo "    19-10 B Medium"
	@echo "   100-20 A Very high"
	radon mi $$(find . -name '*.py') --show 
	@echo
	@echo " ======== raw: Raw metrics ========"
	@echo "Output interpretation:"
	@echo "LOC: the total number of lines of code"
	@echo "LLOC: the number of logical lines of code"
	@echo "SLOC: the number of source lines of code - "
	@echo "  not necessarily corresponding to the LLOC"
	@echo "Comments: the number of comment lines"
	@echo "    (i.e. only single-line comments #)"
	@echo "multi: the number of lines representing multi-line strings"
	@echo "blank: the number of blank lines (or whitespace-only ones)"
	@echo ============================================
	radon raw $$(find . -name '*.py') --summary
	@echo
	@echo  ======== hal: Halstead complexity metrics ========
	@echo "    Computed on the function level as opposed to file level"
	radon hal $$(find . -name '*.py') --functions
	@echo
	@echo  ======== prospector: Prospector ========
	prospector

# Use pandoc for formating md -> html.
doc:
	pandoc -s --toc README.md -o README.html
	@echo "Load the html file into your browser with this URL:"
	@echo "    file://$(PWD)/README.html"

# Due to difficulties in creating a package, kludge cleaning out the
# code to track difficulties and installing in various ways.
clean:
	$(RM) -rf dist htmlcov build speed_math.egg-info
	find . -name logs.log -delete
	find . -name '*.pyc' -delete
	find . -name '*.pyo' -delete
	$(PYTHON) setup.py clean

