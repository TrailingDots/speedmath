---
title: Speed Math Command Line Trainer - version 0.2
---

# "Instant" Start

~~~ bash
    pip install deepmerge # install required package
    cd <your_root>        # cd to a convient folder for speed_math
    git clone https://gitlab.com/TrailingDots/speedmath.git
    cd speed_math
    # Try out a few runs:
    ./speed_math.sh --help      # Display help
    # OR:
    ./speed_math.sh --addition -c add_3.yaml
    ./speed_math.sh -a         -c add_3.yaml  # Use short flags
    ./speed_math.sh --multiplication -c mult_11_12.yaml
    ./speed_math.sh -m -c mult_11_12.yaml     # Use short flags
    # OR:
    ./speed_math.sh --list       # List pre-built configurations
~~~

## Requirements

Python 3.6+ is required. 
This code likely works in a
3.1+ but is untested for these python 3 releases.

Python 3+ should be loaded with your virtual environment. 

Pre-install requirements:

~~~ bash
    pip install deepmerge            # To merge config files
    alias python=python3             # Force use of python3
~~~

Point your browser to:
[speed_math](https://gitlab.com/TrailingDots/speedmath).
Then select your preferred download options from the
"clone" dropdown. I like the http version myself.

This is how an install and quick checkout should proceed.
Normal output is suppressed.

~~~ bash
    cd <speed_math base dir>    # Your choice
    git clone https://gitlab.com/TrailingDots/speedmath.git

    cd speed_math       # Into speed_math main directory
    ./speed_math.sh --help     # List help. Contains sample runs.

    # Try a simple addition exercise, Ctrl-C to terminate if desired.
    ./speed_math.sh -a -c add_3.yaml

    # Notice the results get logged for later training graphs.
    # This file may be loaded into a spreadsheet for graphics trends.
    cat speedy.csv
    timestamp,config,addition,right,wrong,seed,time
    20210308T084112,add_3.yaml,addition,7,3,80.10
~~~

# Who is this `speed_math` trainer for?

Speed math training is a great way to keep your mind
sharp. This program helps in brushing up with sums,
product, subtraction and division.

Simple user configuration files allow settings from very
simple to world class problems.

Any mathematics system such as Trachtenberg or
Vedic or "normal" school math may be used.
Unfortunately, specialized support for the
various systems is not present.

Anyone desiring arithmetic practice will profit by mental
math practice. While becoming a "numbers genius"
requires dedication and work, using problem
sets from this program can benefit most
practitioners.

The only way to become great at mental math is
to practice. And this program provides great
practice sessions that scale from your current
level to world class.

# Features

None of these pros or cons apply if you do not practice
on a regular basis. To improve your arithmetic skills
takes dedication and practice. A software package
merely assists and cannot replace.

`speed_math` does not use the [spaced repetition
system](https://en.wikipedia.org/wiki/Spaced_repetition)
 for learning. Each individual must determine their
own strengths and weaknesses. The spaced repetition
systems apply to learning from flashcards.
This is not useful in learning speed math or mental math.

## Pros:

1. Addition, subtraction, multiplication and division problems.
1. Timings for each session of problems.
1. Scripts allow pre-set runs to easily train over learning sessions.
1. Log file of each session that used to track progress.
1. Log files contain timings and right/wrong values.
1. Log files that can be loaded into a spreadsheet for
   learning curves as progress indicators.
1. Two ways of inputing numbers: left-to-right and right-to-left.
1. Runs in console.
1. Unlimited number of problems.
1. Highly configurable.
1. A random number seed allows redoing a particular session.
1. Can practice Olympiad strength problems for addition, multiplication
   and exact division. (Olympiad does not offer subtraction
   problems)

## Cons:

1. Runs from pre-edited config files. 
   These are easy to create and modify with any text editor.
   This is not necessarily a 'con'.
1. No right-to-left digit entry with "proper" alignment.
   This is the most natural way of entry. See discussion below.
1. For Olympiad training several operators are missing. If
   enough requests are made, these operators can easily
   be coded.

# Setup a new Conda Environment

Create and activate a Speed Math Training environment.
You'll need access to the command line.
(You are welcome to create your own name for the
virtual environment.)

In the terminal window type:

~~~ bash
    conda create -n speedmath python=3.6
~~~

The screen will then display a message that should look similar
to this:

~~~
    The following NEW packages will be INSTALLED:

    <snip for brevity>
    Proceed ([y]/n)?
~~~

Type y and hit return.

Depending on your computer, internet speed, and if we have done
this before, the computer will process for 5 seconds to 5 minutes,
then return us to the command-line. Once that’s done, we type
this at the command-line:

~~~ bash
    conda activate speedmath
~~~

## Virtual Environment Details

Anaconda3 is the tool we use for virtual environments.

Virtual environments allow us to do things like run one project
with Python 3.1 and 3.6 on the same machine.
Python 3.X is required. A virtual environment such as
[anaconda3](https://docs.anaconda.com/anaconda/install/)
is *highly* recommended.

In general, the reference to Anaconda3 is "conda" since "conda"
gets used as the command line command.

## Experienced with Conda Alternatives?

Advanced users might have experience with tools likd
virtualenv, docker, pyenv, etc. If so, use them.

## Installing Conda on Linux

1. Go to [miniconda](https://docs.conda.io/en/latest/miniconda.html#linux-installers)
1. Find the Python 3.6 versions or one close enough.
1. Click the link with 64-bit in the name and download.
1. On your computer, open a terminal. Go to the directory
   where you save the downloaded files: <code>cd ~/Downloads</code>
   (Your directory may be different.)
   Make the file exe-cutable: <code>chmod a+x Miniconda3-latest-Linux-x86_64.sh</code>
   (Your name may be different depending on  your selection.)
   Execute the file: <code>./Miniconda3-latest-Linux-x86_64.sh</code> .
1. Follow the instructions on the screen. If unsure about any
setting, accept the defaults. We can change them later.
1. When the installation is finished, close the current terminal
window and open a new one.
1. Type <code>conda list</code> and press enter.


# Trainer Fast Start

In the base directory of the downloaded source, run
the speed_math.sh script to get the defaults sessions:

~~~ bash
    ./speed_math.sh --addition           # For additional practice
    ./speed_math.sh --multiplication     # For multiplication practice
    # OR:
    ./speed_math.sh -a                   # For additional practice
    ./speed_math.sh -m                   # For multiplication practice
~~~

The above uses default configurations which may or
may not satisfy your needs. Please consider using
one of the existing configuration files or create
your own.

To try a simpler trainer using a named configuration:

~~~ bash
    ./speed_math.sh --config add_3.yaml --addition
    ./speed_math.sh --config add_3.yaml --multiplication
    # OR:
    ./speed_math.sh -c add_3.yaml -a
    ./speed_math.sh -c add_3.yaml -m
~~~

Multiple example configuration files
provide a reasonable range of exercises. 
These are located in the 'speed_math/config' directory.

## Easy Packaging of Custom Configurations

Because a user is expected to practice over extended
periods, a specific exercise can be packaged in 
a linux script:

~~~ bash
    cat >add3       # Use your session name
    #!/bin/bash
    ./speed_math.sh --config add_3.yaml --addition
    Ctl-d           # This terminates the script
    chmod a+x add3  # Make the script executable
    ./add3          # Start using the custom script
~~~

This creates a script file called `add3`. The Ctl-D means
press the control key and the d key to terminate input
to the `cat` command.

Scripts are conveniently located in the main directory
of `speed_math` and not in a config directory. This
allows a user to simply go to the `speed_math`
directory and begin practicing.

<b>Important</b>: a proper configuration file must be placed
in `speed_math/config.add_3.yaml` that is referenced
by the small script above. The newly created `add3`
script references this configuration!

To start the newly created add3 exercises:

~~~ bash
    ./add3
~~~

# Configuration

Using configuration files permits a mental
math practitioner to run the practice
sessions over and over. The history file
of `speedy.csv` provides results of
this training.

Configuration gets loaded from two files:
a default configuration file and a specific 
session configuration file.

The <i>default</i> configuration file contains all configuration
settings to run all operations. 

The <i>user</i> configurations are user created 
and contains all the settings necessary to run 
a specific training session. These user settings override
any settings in the default configuration file.

All configuration files must exist in the `speed_math/config`
directory.

Do *NOT* modify any files in the
`speed_math/tests/` directory!

<i>Tip</i>: To create a new configuration file, copy an existing
config file to a decently named for that copied file.
Modify the 'title' line in the new config file.
Then hack that new configuration file to your desires.

## Default Configuration

The default configuration is named "default.yaml" and
it is strongly recommended that this not be renamed
or modified.

The default configuratin provides settings for each
of the training sessions as well as common settings
used throughout the sessions.

The default configuration, with comments omitted, looks like:

~~~ yaml
    cat default.yaml            # Print the default.yaml file
    common:
        timing: True            # Always time a session.
        reverse_input: True     # Default: Enter numbers right-to-left
        log_file: 'speedy.csv'  # Log file. If omitted, no log

    addition:                   # Addition of numbers
        wide: 6                 # 6 columns of digits
        rows: 2                 # 2 rows to add
        loops: 10               # Loop for 10 problems

    subtraction:                # Subtraction
        minuend_wide: 5         # The number to be subtracted, # digits
        subtrahend_wide: 3      # The number subtracted, # digits
        loops: 10               # Loop for 10 problems

    multiplication:             # Multiply two numbers loops times
        multiplicand_wide: 2    # Number of digits in multiplicand
        multiplier_wide: 2      # Number of digits in multiplies
        loops: 10               # Loop for 10 problems

    division:
        dividend_wide: 3
        divisor_wide: 1
        #choices: 7,6           # Practice division by 6 or 7 only.
        loops: 10
~~~

The default config file is always named 'default.yaml'.

A user may override this default file name by using
the `--default_config_file new_default.yaml` where
`new_default.yaml` is the your newly provided 
default config file.

Yaml encoding is commonly used for configuration files.

All configuration files must be properly yaml formatted. An error
in yaml syntax should display the line number of the yaml file.
In case of errors, inspect the previous lines before the 
indicated line number. An error in yaml coding results 
in a program abort.

The default configuration file defines sessions
for all operations. More will be added as necessary.
The settings will not likely satisfy users and
are expected to be overwritten in the user configuration
files.

Creating and running a session with the default
configuration is as easy as:

~~~ bash
    ./speed_math.sh --addition
    # OR:
    ./speed_math.sh -a
~~~

This default is unlikely to satisfy users. Please
create your own session configurations.

## Title for Configuration Files

As a mental math practitioner develops, keeping track
of each configuration can be a chore. To ease this,
use the `title:` in each created configuration file.

A keyword of "title" permits a quick reminder of what
the file does. As the number of your config files increases,
this helps in a quick determination of how to use
the config file.

An example of a title in a configuratino file:

~~~ bash
    #
    # To run addition sessions:
    # ./speed_math.sh --config add_3.yaml --addition
    #
    # To run multiplication session:
    # ./speed_math.sh --config add_3.yaml --multiplication
    #
    title: "10 loops. Add 3 digits wide, 2 rows. Multiply: 2 by 2"
    # Present 10 problems.
    addition:
        wide: 3
        rows: 2
        loops: 10

    # Multiplication: 2 digits wide each number
    multiplication:
        multiplicand_wide: 2
        multiplier_wide: 2
        loops: 10
~~~

This config file first lists its use for adding or multiplying
from the command line.

The "title:" quickly describes the intended usage.

Now the addition and multiplication specify how problems get presented.

Notice that multiple operators my be configured
in each configuration file.

### Details of Default Configuration

Configuration files use [yaml](https://en.wikipedia.org/wiki/YAML)
for their settings.

Yaml is a human friendly data serialzation
standard. Yaml is easy to read and understand.
Modification to configuration files should thus be
easy.

The default configuration file contains settings for all
operators. 

User supplied configuration files may supply setting
different than the default configuration and thusly
override the default configuration.

A training session without a user config file may or may
not suit your expectations. A user config file is highly
recommended.

Name your user config files to something memorable.
Create your user config file by copying an existing config
file and modifiing according to your desires.

### 'title' Line

Each config file should have a title that describes the problem
configuration. This should not extend more than one line.

To get a list of title in all know config files:

~~~ bash
    ./speed_math.sh --list
~~~

### 'common' Configuration

The common section contains settings used for each
operation.

~~~ python
    common:
        log_file: 'speedy.csv'  # Log file. If omitted, no log
        reverse_input: True     # Default: Enter numbers right-to-left
~~~

log_file
:   The name of the file to save scores for each session.
    If omitted, sessions save results in `speedy.csv`.

reverse_input
:   This setting determines whether input numbers get
    entered right-to-left (rtl) or left-to-right (ltr).
    When performing mental math, the usual entries
    get written from right to left. Unfortunately
    terminal entry is always ltr. Using
    "reverse_input: True" decreases the mental load
    of remembering the digits as a user had to
    be concerned with only a single column at a time.
    With "reverse_input: False", a user must remember
    the result of each column and reverse this
    sequence for a final answer. Terminal input
    is not as flexible as web-based input.
    See the section further on for examples.

### addition

The addition settings control the addition sessions.

~~~ python
    addition:                   # Addition of numbers.
        wide: 6                 # Each integer is 6 digits wide.
        rows: 2                 # 2 rows to add.
        loops: 10               # Loop for 10 problems.
~~~

wide
:    The number of digits on each row for addition.
     For the value of 6, the numbers have a range of 
     100000 thru 999999 inclusive. For a value of 3,
     the numbers have a range from 100 to 999.
     For the value of 2, the numbers have a range 
     of 10 thru 99 inclusive.

rows
:    The number of rows in each problem to add.

loops
:    The number of problems in each session.

### subtraction

The subtraction section provides settings to control
subtraction.

~~~ yaml
    title: "Subtraction: 5 digits minus 3 digit numbers. 3 loops"
    subtraction:
        minuend_wide: 5
        subtrahend_wide: 3
        loops: 3
~~~

minuend_wide
:   The number of digits in the minuend (upper value)

subtrahend_wide
:   The number of digits in the subtrahend (lower value)

choices
:   Each subtrahend gets constrained to a random value in
    this comma seperated list.

#### Formal naming of subtraction

The number subtracted from is the <b>minuend</b>. 
The number being subtracted is know as the <b>subtrahend</b>.
The result is the <b>difference</b>.

~~~ text
    123456    # Minuend
       789    # Subtrahend
    ------
    122667    # Result
~~~

### multiplication

The multiply section provides settings to control multiplication.

~~~ python
    multiplication:             # Multiply two numbers loops times
        multiplicand_wide: 2    # Number of digits in multiplicand
        multiplier_wide: 2      # Number of digits in multipliers
        choices: 7,11           # OPTIONAL: Multiply by 7 or 11 randomly
        loops: 10               # Loop for 10 problems
~~~

multiplicand_wide
:    The number of digits in the multiplicand. (The top
     number in a multiplication problem.)
     For the value of 2, the numbers have a range of 10 thru 99 inclusive.

multiplier_wide
:    The number of digits in the multiplier.
     For the value of 2, the numbers have a range of 10 thru 99 inclusive.

choices
:    This <i>optional</i> setting requires a list of numbers
     that will be used as multipliers. The above setting of
     "7,11" will use _only_ 7 or 11 for multipliers.

     This setting is <i>great</i> for practicing multiplication
     by only one number. For example, `choices: 7` allows
     practice of multiplication for all problems by 7.

     If no choices keyword appears, then the multiplier_wide
     will contain the number of digits specified by the
     multiplier_wide setting.

     "choices", if present, overrides the "multiplier_wide"
     setting.

loops
:    The number of problems in each session.

### division

The division section provides settings to control division.
The values for division get constrained for exact integers only.
Results are always integer results with no fractions.

~~~ yaml
    title: "Divide 3 digits dividend, by 7 or 6. 10 loops"
    common:
        reverse_input: False # left to right for div
    division:
        dividend_wide: 3
        divisor_wide: 1
        choices: 7,6
        loops: 10
~~~

common
:   Ensure this `reverse_input: True` gets set for division.
    Normal hand division enters digits left to right.

dividend_wide
:   The number of digits in the larger number.

divisor_wide
:   The number of digitis in the smaller number to divide into
    the larger number.

## User Created Configuration 'Script' Files

Running the trainer with only the default configuration
and no session configuration file results in default
problems. These may be unlikely to suit your level as
either too hard or too easy.

An example of running a session with a config file could be:

~~~ bash
    ./speed_math.sh --config mult_11_12.yaml --multiplication
    # OR:
    ./speed_math.sh -c mult_11_12.yaml -m
~~~

The example configuration file <code>mult_11_12.yaml</code>
illustrates the addition of <code>choices</code> to the
multiplication problems:

~~~ yaml
    multiplication:
        multiplicand_wide: 2
        multiplier_wide: 2  # This line ignored since choices exists.
        choices: 11,12      # Multiply by 11 or 12 only.
        loops: 10
~~~

Notice that this config file had no "common" settings
because it inherits "common" settings from the "default.yaml"
config file.

In this multiplication session the choices for the multiplier
are limited to either 11 or 12. 

Problems with this user configuration
limit themselves to 2 digit numbers multiplied by
either 11 or 12 with 11 or 12 randomly selected as the
multiplier.

If <code>choice: 7</code>,
then then multiplier is always 7. In this case, the session
provides problems targeted to multiplication by 7 only.

If you want to practice with specific numbers only, this
is a great way to create problems.

Use "choices" to practices on specific areas such a
multiplications by 11, 12, 15, 25, etc.

For the above multiplication yaml configuration, the
session consists of 10 problems of 2 digits multiplied
by 7.

### Multple Operations per Config File

The yaml configuration file <code>add_3.yaml</code> illustrates
that a config file may configure multiple operations
for practice sessions. While the name "add_3.yaml" used as
multiplication is misnamed, this illustrates a possiblity.

~~~ yaml
    # Addition: 3 digits wide, 2 rows
    # Present 10 problems.
    addition:
        wide: 3
        rows: 2
        loops: 10

    # Multiplication: 2 digits wide each number
    multiplication:
        multiplicand_wide: 2
        multiplier_wide: 2
        loops: 10
~~~

The above starts a multiplication session with:

~~~ bash
    ./speed_math.sh --config add_3.yaml --multicatin
~~~

An additional session would be:

~~~ bash
    ./speed_math.sh --config add_3.yaml --addition
~~~

# Right-to-left or Left-to-right?

Notice the setting in the default.yaml file of
<code>reverse_input: True</code>.

This setting controls the handling of user input answer.
Specifically this controls right-to-left (rtl) input
vs left-to-right (ltr) input.

When performing addition or multiplication the
numeric digit entry usually gets entered from the
right end of the answer. 

As an example with <code>reverse_input: True</code>,
consider adding two digit numbers:

<pre>
     123
     456
    ----
</pre>

The first digit is 9 = 3 + 6 and the display becomes:

<pre>
     123
     456
    ----
    9   
</pre>

The second digit is 7 = 3 + 5 and the display becomes:

<pre>
     123
     456
    ----
    97
</pre>

The last digit is 5 = 1 + 4.
So the final result looks like:

<pre>
     123
     456
    ----
    975
</pre>

This display is *not* the normal way of displaying an answer.

On web pages this gets handled with internationalization (I18N)
settings.

On Linux terminals these numeric entries operate the same
as any other strings: left-to-right.

TBD on some future release.

## Even more practice with left-to-right entry!

Setting <code>reverse_input: False</code> requires a user
to get the final answer and input in the normal sequence
of digits: left-to-right.

This has an advantage of building even more speed math
skills by remembering the digits as calculated, then reversing
those digits for a final answer.

Specifically, when mentally computing an answer in front of
other people, they expect the hear the answer in
a left-to-right sequence.

# Log File Format

A log file, `speedy.csv`, uses a series of comma separated
variables (CSV) lines that record results of
each session.

The csv file may be removed, edited and/or read by 
a spreadsheet as desired. This file will be 
auto-created if it does not exist.

The initial line of this CSV file contains the column
names and should be self-explanatory.

A leading timestamp records all log entries. ISO 8601
provides the of the format. `speed_math` uses the
following timestamp:

~~~ python
    %Y%m%dT%H%M%S
~~~

This format gets interpreted as: Year-Month-Day Hour-Minute-Second.
[ISO Time (ISO 8601) in
Python?](https://exceptionshub.com/iso-time-iso-8601-in-python.html)
provides a simple explanation.

The location of this format is in `speed_math/speed_math.py`
may be modified if loading to your spreadsheet
encounters problems. After modifying the timestamp format,
remove the `speedy.csv` file and speed_math will write a
new header and begin recording is the new format.

# To Be Done

Updates will depend on feedback.

1. Add operations for division, subtraction, squares, roots.
1. Get pythonic user installation working properly. 
   <code>sdist</code> - Yuck! Python packaging sucks. Pyinstall?
1. NCurses of right-to-left numeric entry on linux consoles. Clumsy?
1. A web based version? Write in Django? NCurses?
1. Detailed notes on anaconda3 use, installation procedures.
1. Operations: Add subtraction, division, roots. Dates? Powers?
1. Dates? Does anyone need date exercises?
1. The current display shows vertical problems only.
   Show multiplication problems horizontally?
1. Dedicated directory from $HOME for config files?
1. Division anyone? Not too difficult.
1. Subtraction anyone? Should be easy.
1. Developer notes? 
1. Roots? h
1. The Makefile does not have all targets working correctly. Important?
1. "make lint" requires other utilities that may not be in a 
   users environment.  Important?
1. Straighten out the imports. I don't like to write os.path.append() 
   to get an import to work. The python import system is confusingly broken.
1. Random numbers get generated by the OS - linux. Is it necessary to allow
   users to seed a generator for repeatable problems?
1. A CSV, Comma Separated Variables, file gets generated with each example  run.
1. What is the interest in generating progress reports? Other than creation,
nothing else happens to this file.
1. Forget the entire config philosophy and prompt the user for
all config options each time the code runs? Some command line utilities
make this "easy"?
1. Division results in exact integers by design. If users
   desire, an inexact division to X decimal places?

## Mental Calculations World Championship

[World Mental
Calculation](https://worldmentalcalculation.com/mental-calculations-world-championship/)
offers competitions in categories this packages does not
cover:

-    addition, subtraction, multiplication and division
-    exact division
-    irrational and exact square roots, cube roots and deeper roots
-    factorizations
-    fractions
-    calendar dates
-    surprises

# Change Log


## Version 0.4

Added division. Converted to tuples for easire handling
of errors and results. This is a step towards using
python monads without the ensuing complexity.

The test suite was beefed up and coverage is 95%.

Many refactoring in various places.

## Version 0.3

Added division, subtraction. Added stubs for squares and roots.

### Branch to add operators

Use git branch AddOperators for division, subtraction, roots

### Branch Tests

Adding tests to speed_math

## Version 0.2

Major renaming and re-org of code. Moved from an initial assumption
of Trachtenberg based training to any mental math training.
Users are free to use any math system they like - hopefully!

Attemped to use package for pip, but don't have enough time
to figure out the nuances.

This means users must run the code in the source code directory -
not good, but not too bad.

Has 93% code coverage. Not too bad, could be better.

Various lints report (hopefully) minor problems that all
lints nag about.

## Version 0.1

Initial release. Works well, but has rough spots.


# Links

Lots of places have documentation/videos on speed math. This
abundance leaves me in awe and so I do not provide
links on speed math.

If you desire to learn speed math or mental math, practice
is the only way to obtain decent results.

If you are serious:

[Top 5 Mental Math Methods in the
World](https://ezinearticles.com/?Top-5-Mental-Math-Methods-in-the-World&id=549256)
reviews five speed mental math systems from the normal method taught in
shcol to Trachtenberg and Vedic. He states that the speed Vedic math is
the best.

[vedic Maths India](https://vedicmathsindia.org/) organizes the Vedic
math system with courses, franchises, etc. They state Vedic mental
math is the world's fastest math system.

[The Kumon Method and its
Strengths](https://www.kumon.com/about-kumon/kumon-method) offers
a Japanese method. Deeply disappointing is the fact that The Kumon
Method is not mental at all nor does it offer any special methods
and does not improve one's speed.

[The Trachtenberg Speed System of Basic Mathematics](https://www.amazon.com/s?k=trachtenberg+speed+system+of+basic+mathematics&crid=2BVL2UV9RFUGW&sprefix=trachtenberg+speed+system+of+basic+mathematics%2Caps%2C260&ref=nb_sb_ss_ts-doa-p_1_46)
describes the famous Trachtenberg speed mathematics. This book contains
the interesting history of this system wherein the originator
developed it in German concentration camps. For other expositions
of this system google: "Trachtenberg speed math". 

See also
[The Trachtenberg Speed System of Basic
Mathematics](https://trachtenbergspeedmath.com/) that contains
many details of this system. A <b>free</b> pdf download is included.
If enough interest in this systems exists, I may
extend this software to cover all that Trachtenberg describes.

