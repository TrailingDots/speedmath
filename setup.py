import os
from setuptools import setup

thelibFolder = os.path.dirname(os.path.realpath(__file__))
requirementPath = thelibFolder + "/requirements.txt"
install_requires = ['deepmerge']
if os.path.isfile(requirementPath):
    with open(requirementPath) as f:
        install_requires = f.read().splitlines()

setup(name="speed_math",
    version="0.3.0",
    author="Cecil McGregor",
    author_email="TrailingDots@gmail.com",
    maintainer="Trailing Dots",
    maintainer_email="TrailingDots@gmail.com",
    url="git@gitlab.com:TrailingDots/consolespeedmath.git",
    description="Trainer for mental math",
    long_description="""
        Train for mental math with addition, multiplication,
        history logs, timings, flexible exercises""",
    download_url="git@gitlab.com:TrailingDots/consolespeedmath.git",
    keywords=["math", "mental-math", "speed-math"],
    license="MIT",
    packages=['speed_math'],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.1",
        "Programming Language :: Python :: 3.2",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Intended Audience :: End Users/Desktop",
        "Intended Audience :: Education",
        "Intended Audience :: Other Audience",
        "License :: OSI Approved :: MIT",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Mathematics"
    ],
    install_requires=install_requires,
)
